package model;

import java.util.Date;

public class Transaction {
    private int id;
    private float montant;
    private int compte_source;
    private int compte_destination;
    private Date date;
    private String libelle;
}
