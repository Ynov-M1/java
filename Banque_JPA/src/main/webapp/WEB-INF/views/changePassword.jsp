<%--
  Created by IntelliJ IDEA.
  User: antoinepoussard
  Date: 12/10/2018
  Time: 15:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<fmt:setBundle basename = "main.webapp.internationalization.i18n" />
<html>
<head>
    <title>Comptes</title>
    <link rel = "stylesheet"
          href = "https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel = "stylesheet"
          href = "https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/css/materialize.min.css">
    <script type = "text/javascript"
            src = "https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src = "https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js">
    </script>

</head>
<body>
<jsp:include page="../../views/loaders/navbar.jsp"></jsp:include>

<h1>Bonjour,<c:out value="${client.prenom}"/> <c:out value="${client.nom}"/></h1>


<div class="container">
    <form id="transactionsForm" action="/secu/changePassword" method="post">
        <div class="row">
            <div class="input-field col s6">
                <input name="oldpassword" placeholder="Old Password" id="oldpassword" type="password">
                <label for="oldpassword">Enter your old password</label>
            </div>
            <div class="input-field col s6">
                <input name="newpassword" placeholder="New Password" id="newpassword" type="password">
                <label for="newpassword">Enter the new password</label>
            </div>
        </div>

        <button class="btn waves-effect waves-light blue darken-3" type="submit" name="action">Submit
            <i class="material-icons right">send</i>
        </button>

    </form>
</div>
</body>
</html>
