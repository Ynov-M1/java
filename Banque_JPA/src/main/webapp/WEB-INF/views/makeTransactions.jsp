<%--
  Created by IntelliJ IDEA.
  User: antoinepoussard
  Date: 12/10/2018
  Time: 15:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<fmt:setBundle basename = "main.webapp.internationalization.i18n" />
<html>
<head>
    <title>Comptes</title>
    <link rel = "stylesheet"
          href = "https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel = "stylesheet"
          href = "https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/css/materialize.min.css">
    <script type = "text/javascript"
            src = "https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src = "https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js">
    </script>

    <script>

        (function($){

            $(function(){
                // Plugin initialization
                $('select').not('.disabled').formSelect();

            });
        })(jQuery); // end of jQuery name space

    </script>
</head>
<body>
<jsp:include page="../../views/loaders/navbar.jsp"></jsp:include>

<h1>Bonjour,<c:out value="${client.prenom}"/> <c:out value="${client.nom}"/></h1>


<div class="container">
    <form id="transactionsForm" action="/secu/maketransactions" method="post">
        <div class="row">
            <div class="input-field col s4">
                <select  name="from" form="transactionsForm">
                    <option value="" disabled selected>Choose your option</option>
                    <c:forEach items="${client.comptes}" var="item">
                        <option value="<c:out value="${item.id}"/>"><c:out value="${item.libelle}"/></option>
                    </c:forEach>
                </select>
                <label>Select your account</label>
            </div>
            <div class="align-items-center col s4">
                <p style="text-align: center; vertical-align: middle;"><i class="material-icons">send</i></p>
            </div>

            <div class="input-field col s4">
                <input name="to" placeholder="Account to be credited" id="account_credited" type="number" step="0.01" data-validate="Please enter account id">
                <label for="account_credited">Enter the number of the account to be credited</label>
            </div>

            <div class="input-field col s6">
                <input name="amount" placeholder="Amount" id="amount_money" type="number" step="0.01" data-validate="Please enter amount">
                <label for="amount_money">Amount</label>
            </div>

            <div class="input-field col s6">
                <input name="libelle" placeholder="Libelle" id="lib" type="text" data-validate="Please enter name">
                <label for="lib">Libelle</label>
            </div>
        </div>

        <button class="btn waves-effect waves-light blue darken-3" type="submit" name="action">Submit
            <i class="material-icons right">send</i>
        </button>

        <c:if test="${errorMsg != null}">
            <p style="color: red"> <c:out value="${errorMsg}"/> </p>
        </c:if>

    </form>
</div>
</body>
</html>
