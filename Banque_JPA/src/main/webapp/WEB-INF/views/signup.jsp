<%--
  Created by IntelliJ IDEA.
  User: antoinepoussard
  Date: 12/10/2018
  Time: 11:39
  To change this template use File | Settings | File Templates.
--%>
<jsp:include page="../../views/loaders/jstlLoader.jsp"></jsp:include>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Sign Up</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <jsp:include page="../../views/loaders/cssLoader.jsp"></jsp:include>
</head>
<body>
<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <form class="login100-form validate-form p-l-55 p-r-55 p-t-178" action="/signup" method="post">
					<span class="login100-form-title">
						Sign Up
					</span>

                <div class="wrap-input100 validate-input m-b-16" data-validate="Please enter firstname">
                    <input class="input100" type="text" name="Firstname" placeholder="Firstname" required>
                    <span class="focus-input100"></span>
                </div>

                <div class="wrap-input100 validate-input m-b-16" data-validate="Please enter lastname">
                    <input class="input100" type="text" name="Lastname" placeholder="Lastname" required>
                    <span class="focus-input100"></span>
                </div>

                <div class="wrap-input100 validate-input m-b-16" data-validate="Please enter email">
                    <input class="input100" type="text" name="Email" placeholder="Email" required>
                    <span class="focus-input100"></span>
                </div>

                <div class="wrap-input100 validate-input m-b-16" data-validate="Please enter login">
                    <input class="input100" type="text" name="Username" placeholder="Username" required>
                    <span class="focus-input100"></span>
                </div>

                <div class="wrap-input100 validate-input m-b-16" data-validate = "Please enter password">
                    <input class="input100" type="password" name="Password" placeholder="Password" required>
                    <span class="focus-input100"></span>
                </div>

                <div class="wrap-input100 validate-input m-b-16" data-validate="Please enter address">
                    <input class="input100" type="text" name="Address" placeholder="Address" required>
                    <span class="focus-input100"></span>
                </div>

                <div class="wrap-input100 validate-input m-b-16" data-validate="Please enter phone">
                    <input class="input100" type="tel" name="Phone" placeholder="Phone" required>
                    <span class="focus-input100"></span>
                </div>

                <div class="wrap-input100 validate-input m-b-16" data-validate="Please enter birthdate">
                    <input class="input100" type="date" name="Birthdate" placeholder="Birthdate" required>
                    <span class="focus-input100"></span>
                </div>

                <div class="text-right p-t-13 p-b-23">
						<span class="txt1">
							Already have an account?
						</span>

                    <a href="/login" class="txt2">
                        Sign In
                    </a>
                </div>

                <c:if test="${errorMsg != null}">
                    <p style="color: red"> <c:out value="${errorMsg}"/> </p>
                </c:if>

                <div class="container-login100-form-btn flex-col-c p-t-70 p-b-40">
                    <input type="submit" value="Sign Up" class="login100-form-btn">
                </div>


            </form>
        </div>
    </div>
</div>

<jsp:include page="../../views/loaders/scriptLoader.jsp"></jsp:include>


</body>
</html>