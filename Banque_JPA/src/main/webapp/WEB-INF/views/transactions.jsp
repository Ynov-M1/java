<%--
  Created by IntelliJ IDEA.
  User: antoinepoussard
  Date: 12/10/2018
  Time: 15:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<fmt:setBundle basename = "main.webapp.internationalization.i18n" />
<html>
<head>
    <title>Comptes</title>
</head>
<body>
<jsp:include page="../../views/loaders/navbar.jsp"></jsp:include>



<%
    String getURL=request.getRequestURL().toString();
%>

<h1>Bonjour,<c:out value="${client.prenom}"/> <c:out value="${client.nom}"/></h1>

<div class="container">
    <h4>Solde Actuel: <c:out value="${comptes.solde}"/> €</h4>
    <%--<c:url value="/secu/transactions" var="actionURI">--%>
        <%--<c:param name="id" value="${id}"/>--%>
    <%--</c:url>--%>

    <display:table name="comptes.transactions" sort="list" pagesize="10" requestURI="/secu/transactions" keepStatus = "true" class="striped centered responsive-table" >
        <display:column property="libelle" title="Libelle" />
        <display:column property="montant" title="Montant"  />
    </display:table>

    <%--<table class="striped centered responsive-table" >--%>
        <%--<thead>--%>
        <%--<tr>--%>
            <%--<th>Libelle</th>--%>
            <%--<th>Montant</th>--%>
        <%--</tr>--%>
        <%--</thead>--%>

        <%--<tbody>--%>
        <%--<c:forEach items="${comptes.transactions}" var="item">--%>
            <%--<tr>--%>
                <%--<td><c:out value="${item.libelle}"/></td>--%>
                <%--<td><c:out value="${item.montant}"/> €</td>--%>
            <%--</tr>--%>
        <%--</c:forEach>--%>

        <%--</tbody>--%>
    <%--</table>--%>
</div>
</body>
</html>

<style>
    .pagebanner{
        display: none;
    }
</style>
