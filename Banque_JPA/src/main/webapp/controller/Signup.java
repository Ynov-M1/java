package main.webapp.controller;

import main.webapp.manager.ClientManager;
import main.webapp.model.Client;
import main.webapp.utils.Utils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@WebServlet("/signup")
public class Signup extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/views/signup.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher signupDispatcher = getServletContext().getRequestDispatcher("/WEB-INF/views/signup.jsp");
        String Firstname = req.getParameter("Firstname");
        String Lastname = req.getParameter("Lastname");
        String Username = req.getParameter("Username");
        String Password = req.getParameter("Password");
        String Address = req.getParameter("Address");
        String Phone = req.getParameter("Phone");
        String Date = req.getParameter("Birthdate");
        String Email = req.getParameter("Email");

        String errMsg = checkSignUpForm(Firstname, Lastname, Username, Password, Address, Phone, Date, Email);
        if(errMsg != null){
            req.setAttribute("errorMsg", errMsg);
            signupDispatcher.forward(req, resp);
        } else {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.FRANCE);
            try {
                Date bod = format.parse(Date);
                Client newClient = new Client(Lastname, Firstname, Email, Username, Password, Phone, bod, Address);
                ClientManager.saveClient(newClient);
                resp.sendRedirect(req.getContextPath() + "/login");
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }


    }

    private String checkSignUpForm(String firstname, String lastname, String login, String password, String Address, String Phone, String Date, String Email){
        Utils utils = new Utils();
        if (firstname == null || firstname.length() == 0) {
            return "Le champ Firstname est obligatoire";
        } else if (lastname == null || lastname.length() == 0) {
            return "Le champ Lastname est obligatoire";
        } else if (login == null || login.length() == 0) {
            return "Le champ Login est obligatoire";
        } else if (password == null || password.length() == 0) {
            return "Le champ Password est obligatoire";
        } else if (Address == null || Address.length() == 0) {
            return "Le champ Address est obligatoire";
        } else if (Phone == null || Phone.length() == 0) {
            return "Le champ Phone est obligatoire";
        } else if (Date == null || Date.length() == 0) {
            return "Le champ Date est obligatoire";
        } else if (Email == null || Email.length() == 0) {
            return "Le champ Email est obligatoire";
        } else if (utils.checkPassword(password)){
            return "Le mot de passe doit faire au moins 8 caracteres, avoir au moins un numero, une majuscule et un caractere special";
        } else {
            return null;
        }
    }
}
