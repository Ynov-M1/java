package main.webapp.controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.jstl.core.Config;
import java.io.IOException;

@WebServlet("/i18n")
public class i18n extends HttpServlet {
    private String lang = "fr_FR";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String paramValue = req.getParameter("lang");
        String redirect = req.getParameter("from");
        if (paramValue == null || paramValue == "") {
            Config.set(req.getSession(), Config.FMT_LOCALE, new java.util.Locale("fr", "FR"));
        } else {
            Config.set(req.getSession(), Config.FMT_LOCALE, new java.util.Locale(paramValue, "FR"));
        }
//        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(redirect);
//        dispatcher.forward(req, resp);
        resp.sendRedirect(req.getContextPath() + redirect);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

    public String getLang() {
        return lang;
    }
}
