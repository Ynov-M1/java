<%--
  Created by IntelliJ IDEA.
  User: antoinepoussard
  Date: 12/10/2018
  Time: 12:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<fmt:setBundle basename = "i18n" />
<html>
<head>
    <title>Home</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Ubuntu:regular,bold&subset=Latin">
</head>
<body>
<ul id="dropdown1" class="dropdown-content">
    <li><a href="<c:url value="/i18n">
            <c:param name="lang" value="fr"/>
            <c:param name="from" value="${requestScope['javax.servlet.forward.request_uri']}?${requestScope['javax.servlet.forward.query_string']}"/>
        </c:url>">FR</a></li>
    <li class="divider"></li>
    <li><a href="<c:url value="/i18n">
            <c:param name="lang" value="en"/>
            <c:param name="from" value="${requestScope['javax.servlet.forward.request_uri']}?${requestScope['javax.servlet.forward.query_string']}"/>
        </c:url>">EN</a></li>
</ul>

<nav>
    <div class="nav-wrapper" style="background-color: #3561b8; text-align: center; font-family: Ubuntu;">
        <a href="/home" class="brand-logo" style="text-align: center"><fmt:message key="Titre"/></a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a class="dropdown-trigger" href="#!" data-target="dropdown1"><fmt:message key="ChoixLangue"/><i class="material-icons right">arrow_drop_down</i></a></li>
            <c:choose>
                <c:when test="${client == '' || client == null}">
                    <li><a href="/login">Login</a></li>
                </c:when>
                <c:otherwise>
                    <li><a href="/logout">Logout</a></li>
                </c:otherwise>
            </c:choose>
        </ul>
    </div>
</nav>

<h1><c:out value="${i18n.lang}"/></h1>
<!-- Compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.dropdown-trigger');
        var instances = M.Dropdown.init(elems);
    });
</script>
</body>
</html>