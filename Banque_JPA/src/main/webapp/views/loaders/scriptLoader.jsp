<%--
  Created by IntelliJ IDEA.
  User: antoinepoussard
  Date: 12/10/2018
  Time: 12:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!--===============================================================================================-->
<script src="../views/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="../views/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="../views/vendor/bootstrap/js/popper.js"></script>
<script src="../views/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="../views/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="../views/vendor/daterangepicker/moment.min.js"></script>
<script src="../views/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="../views/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="../views/js/main.js"></script>
