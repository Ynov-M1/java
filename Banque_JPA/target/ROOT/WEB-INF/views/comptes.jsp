<%--
  Created by IntelliJ IDEA.
  User: antoinepoussard
  Date: 12/10/2018
  Time: 15:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<fmt:setBundle basename = "i18n" />
<html>
<head>
    <title>Comptes</title>
</head>
<body>
    <jsp:include page="../../views/loaders/navbar.jsp"></jsp:include>

    <h1>Bonjour,<c:out value="${client.prenom}"/> <c:out value="${client.nom}"/></h1>

    <div class="container">
        <table class="striped centered responsive-table" >
            <thead>
            <tr>
                <th>Compte</th>
                <th>Solde</th>
            </tr>
            </thead>

            <tbody>
            <c:forEach items="${client.comptes}" var="item">
                <tr>
                    <td><c:out value="${item.libelle}"/></td>
                    <td><c:out value="${item.solde}"/> €</td>
                    <td><a class="waves-effect waves-light btn blue darken-3"
                           href="<c:url value="/secu/transactions">
                                    <c:param name="id" value="${item.id}"/>
                                 </c:url>">
                        Details<i class="material-icons right">send</i></a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <a class="waves-effect waves-light btn blue darken-3" href="<c:url value="/secu/maketransactions"></c:url>">Virement<i class="material-icons right">send</i></a>
        <a class="waves-effect waves-light btn blue darken-3" href="<c:url value="/secu/account_creation"></c:url>">Creer un compte<i class="material-icons right">send</i></a>
        <a class="waves-effect waves-light btn blue darken-3" href="<c:url value="/secu/changePassword"></c:url>">Changer le mot de passe<i class="material-icons right">send</i></a>
    </div>
</body>
</html>
