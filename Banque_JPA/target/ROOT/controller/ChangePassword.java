package main.webapp.controller;

import main.webapp.model.Client;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static main.webapp.manager.ClientManager.updatePassword;
import static main.webapp.utils.Utils.checkPassword;

@WebServlet("/secu/changePassword")
public class ChangePassword extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/views/changePassword.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/views/changePassword.jsp");
        String oldpassword = req.getParameter("oldpassword");
        String newpassword = req.getParameter("newpassword");
        Client client = (Client) req.getSession().getAttribute("client");

        if(oldpassword.equals(client.getPassword()) && checkPassword(newpassword)){
            updatePassword(client,newpassword);
            req.getSession().setAttribute("client", null);
            resp.sendRedirect(req.getContextPath() + "/login");
        } else if (oldpassword != client.getPassword()){
            req.setAttribute("errorMsg", "Veuillez entrer l'ancien mot de passe");
            dispatcher.forward(req, resp);
        } else if (!checkPassword(newpassword)) {
            req.setAttribute("errorMsg", "Le mot de passe doit faire au moins 8 caracteres, avoir au moins un numero, une majuscule et un caractere special");
            dispatcher.forward(req, resp);
        } else if (oldpassword == null || newpassword == null){
            req.setAttribute("errorMsg", "Veuillez renseigner tous les champs");
            dispatcher.forward(req, resp);
        }
    }
}
