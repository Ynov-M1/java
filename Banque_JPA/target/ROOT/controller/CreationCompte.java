package main.webapp.controller;

import main.webapp.model.Client;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import static main.webapp.manager.ClientManager.loadClientById;
import static main.webapp.utils.Utils.getParamsString;

@WebServlet("/secu/account_creation")
public class CreationCompte  extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/views/accountCreation.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String libelle = req.getParameter("accountName");
        if(libelle == null || libelle == "") {
            req.getSession().setAttribute("errorMsg", "Le champ libelle est obligatoire");
            resp.sendRedirect(req.getContextPath() + "/secu/account_creation");

        } else {
            String URL_API = "/api/comptes";
            URL url = new URL(req.getRequestURL().replace(req.getRequestURL().length() - req.getRequestURI().length(), req.getRequestURL().length(), URL_API).toString());
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");

            Map<String, String> parameters = new HashMap<>();
            parameters.put("id", Integer.toString(((Client) req.getSession().getAttribute("client")).getId())); //client_id
            parameters.put("libelle", libelle);
            parameters.put("solde", "70");

            con.setDoOutput(true);
            DataOutputStream out = new DataOutputStream(con.getOutputStream());
            out.writeBytes(getParamsString(parameters));
            out.flush();
            out.close();
            int status = con.getResponseCode();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer content = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            con.disconnect();

            Client tmpClient = (Client) req.getSession().getAttribute("client");
            Client newClient = loadClientById(tmpClient.getId());
            req.getSession().setAttribute("client", newClient);

            resp.sendRedirect(req.getContextPath() + "/secu/comptes");
        }
    }
}
