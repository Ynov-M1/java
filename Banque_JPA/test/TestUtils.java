import junit.framework.TestCase;
import main.webapp.utils.Utils;
import org.junit.Test;

public class TestUtils extends TestCase {
    @Test
    public void test01incorrectPassword(){
        String password = "coucou";
        Utils utils = new Utils();
        assertEquals(false, utils.checkPassword(password));
    }

    @Test
    public void test02noPassword(){
        String password = "";
        Utils utils = new Utils();
        assertEquals(false, utils.checkPassword(password));
    }

    @Test
    public void test03tooShortPassword(){
        String password = "Test1;";
        Utils utils = new Utils();
        assertEquals(false, utils.checkPassword(password));
    }

    @Test
    public void test04needsNumber(){
        String password = "Test;";
        Utils utils = new Utils();
        assertEquals(false, utils.checkPassword(password));
    }

    @Test
    public void test05needsMaj(){
        String password = "test1;";
        Utils utils = new Utils();
        assertEquals(false, utils.checkPassword(password));
    }

    @Test
    public void test06needsSpecChar(){
        String password = "test1";
        Utils utils = new Utils();
        assertEquals(false, utils.checkPassword(password));
    }

    @Test
    public void test07Noaccent(){
        String password = "test1é;";
        Utils utils = new Utils();
        assertEquals(false, utils.checkPassword(password));
    }

    @Test
    public void test08ValidPassword(){
        String password = "TestPassword1;";
        Utils utils = new Utils();
        assertEquals(true, utils.checkPassword(password));
    }
}
