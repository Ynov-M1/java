package Kata02;

public class Pion {
    private Kata02.Couleur couleur;

    public Pion(Kata02.Couleur couleur) {
        this.couleur = couleur;
    }
    public Couleur getCouleur() {
        return couleur;
    }
}
